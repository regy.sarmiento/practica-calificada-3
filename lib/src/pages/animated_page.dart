
import 'package:flutter/material.dart';

class AnimatedPage extends StatelessWidget {
 @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text('Pagina Animated'),
        ),
        body: Center(
          child: Column (
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/images/animated.png'),
              Text('Estas en la Pagina de Animated'),
              Icon(Icons.donut_large)
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton (
          onPressed: () {Navigator.pop(context);},
          child :  Icon(Icons.arrow_back),
        ),
    );
  }
}