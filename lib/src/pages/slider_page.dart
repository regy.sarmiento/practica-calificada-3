import 'package:flutter/material.dart';

class SliderPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: AppBar(
          title: Text('Pagina Sliders'),
        ),
        body: Center(
          child: Column (
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.asset('assets/images/slider.png'),
              Text('Estas en la Pagina de Sliders'),
              Icon(Icons.sort)
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton (
          onPressed: () {Navigator.pop(context);},
          child :  Icon(Icons.arrow_back),
        ),
    );
  }
}